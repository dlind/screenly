package screenly

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"os"
	"path"
	"strings"
)

type UploadParams struct {
	File string
	StartDate string
	EndDate string
	Duration int
	Server string
}

type EnableAssetRequest struct {
	Name string `json:"name"`
	MimeType string `json:"mimetype"`
	URI string `json:"uri"`
	IsActive bool `json:"is_active"`
	StartDate string `json:"start_date"`
	EndDate string `json:"end_date"`
	Duration int `json:"duration"`
	IsEnabled int `json:"is_enabled"`
	IsProcessing int `json:"is_processing"`
	NoCache int `json:"no_cache"`
	PlayOrder int `json:"play_order"`
	SkipAssetCheck int `json:"skip_asset_check"`
}

type EnableAssetResponse struct {
	ID string `json:"asset_id"`
}

type uploadForm struct {
	form *bytes.Buffer
	contentType string
}

type UploadResponse struct {
	RemotePath string
	Filename string
}

type Client struct {
	httpclient *http.Client
	server string
	v1_url string
	v1_1_url string
}

func withClientOrDefault(c *http.Client) *http.Client {
	if c == nil {
		return http.DefaultClient
	}
	return c
}

func httpOrHttps(https bool) string {
	if https {
		return "https"
	}
	return "http"
}

func NewClient(httpClient *http.Client, server string, https bool) *Client {
	proto := httpOrHttps(https)

	return &Client{httpclient: withClientOrDefault(httpClient),
		server: server,
		v1_url: proto + "://" + server + "/api/v1",
		v1_1_url: proto + "://" + server + "/api/v1.1"}
}

func (c *Client) Delete(id string) error {
	req, err := http.NewRequest("DELETE", c.v1_1_url + "/assets/"+id, nil)
	if err != nil {
		return err
	}

	response, err := c.httpclient.Do(req)
	if err != nil {
		return err
	}

	if response.StatusCode != http.StatusNoContent {
		return fmt.Errorf("error removing %s from %s", id, c.server)
	}

	return nil
}

func (c *Client) Upload(startdate string, enddate string, duration int, file string) (string, error) {
	f, err := os.Open(file)
	if err != nil {
		return "", err
	}

	form, err := writeUploadForm(f)

	response, err := c.httpclient.Post(c.v1_url + "/file_asset", form.contentType, form.form)
	if err != nil {
		return "", err
	}

	defer response.Body.Close()

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	if response.StatusCode != http.StatusOK {
		return "", fmt.Errorf("failed uploading file")
	}

	remotePath := strings.ReplaceAll(string(body), "\"", "")

	assetResponse, err := c.enable(f.Name(), remotePath, startdate, enddate, duration)
	if err != nil {
		return "", err
	}

	return assetResponse.ID, nil
}

func (c *Client) enable(filename string, remotepath string, startDate string, endDate string, duration int) (EnableAssetResponse, error) {
	enableRes := EnableAssetResponse{}

	buf := bytes.Buffer{}

	enableReq := EnableAssetRequest{
		Name:           path.Base(filename),
		MimeType:       "image",
		URI:            remotepath,
		IsActive:       true,
		StartDate:      startDate,
		EndDate:        endDate,
		Duration:      	duration,
		IsEnabled:      1,
		IsProcessing:   0,
		NoCache:        0,
		PlayOrder:      0,
		SkipAssetCheck: 0,
	}

	err := json.NewEncoder(&buf).Encode(enableReq)
	if err != nil {
		return enableRes, err
	}

	response, err := c.httpclient.Post(c.v1_1_url + "/assets", "application/json", &buf)
	if err != nil {
		return enableRes, err
	}

	defer response.Body.Close()

	err = json.NewDecoder(response.Body).Decode(&enableRes)
	if err != nil {
		return enableRes, err
	}

	return enableRes, nil
}

func writeUploadForm(file *os.File) (uploadForm, error) {
	form := uploadForm{}
	buf := bytes.NewBuffer([]byte{})
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return form, err
	}

	mpWriter := multipart.NewWriter(buf)
	defer mpWriter.Close()

	formFile, err := mpWriter.CreateFormFile("file_upload", path.Base(file.Name()))
	if err != nil {
		return form, err
	}

	_, err = formFile.Write(fileBytes)
	if err != nil {
		return form, err
	}

	form.form = buf
	form.contentType = mpWriter.FormDataContentType()

	return form, nil
}

